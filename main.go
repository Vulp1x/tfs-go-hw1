package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

const bitSize int = 64

func main() {
	path := "candles_5m.csv"
	bestTrades, err := getBestTrades(path)

	if err != nil {
		log.Fatalf("Parsing bestTrades caused error: %s", err)
	}

	_, err = parseUserTrades("user_trades.csv", bestTrades)

	if err != nil {
		fmt.Printf("Parsing user trades caused error: %s", err)
	}
}

type BestTrade struct {
	minPrice   float64
	maxPrice   float64
	timeToSell string
	timeToBuy  string
}

func getBestTrades(file string) (map[string]*BestTrade, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("can't open trades file: %s", err)
	}
	defer f.Close()

	csvr := csv.NewReader(f)

	tickers := make(map[string]*BestTrade)

	for {
		row, err := csvr.Read()
		if err != nil {
			if err == io.EOF {
				return tickers, nil
			}

			return tickers, fmt.Errorf("reading newline caused error: %s", err)
		}

		const (
			nameField = iota
			datetimeField
			openPriceField
			maxPriceField
			minPriceField
		)

		minPrice, err := strconv.ParseFloat(row[minPriceField], bitSize)
		if err != nil {
			return nil, fmt.Errorf("failed to parse minPrice: %s", err)
		}

		maxPrice, err := strconv.ParseFloat(row[maxPriceField], bitSize)
		if err != nil {
			return nil, fmt.Errorf("failed to parse maxPrice: %s", err)
		}

		_, ok := tickers[row[nameField]]
		if !ok {
			tickers[row[nameField]] = new(BestTrade)
		}

		updateBestTrade(minPrice, maxPrice, tickers, row)
	}
}

func updateBestTrade(minPrice, maxPrice float64, tickers map[string]*BestTrade, row []string) {
	const (
		nameField = iota
		datetimeField
	)

	if minPrice < tickers[row[nameField]].minPrice || tickers[row[nameField]].minPrice == 0.0 {
		tickers[row[nameField]].minPrice = minPrice
		tickers[row[nameField]].timeToBuy = row[datetimeField]
	}

	if maxPrice > tickers[row[nameField]].maxPrice {
		tickers[row[nameField]].maxPrice = maxPrice
		tickers[row[nameField]].timeToSell = row[datetimeField]
	}
}

func add(m map[int]map[string][]UserTrade, userID int, stockName string, trade UserTrade) {
	mm, ok := m[userID]
	if !ok {
		mm = make(map[string][]UserTrade)
		m[userID] = mm
	}

	mm[stockName] = append(mm[stockName], trade)
}

type UserTrade struct {
	userID     int
	stockName  string
	buyPrice   float64
	sellPrice  float64
	isFinished bool
}

func parseUserTrades(file string, bestTrades map[string]*BestTrade) (map[int]map[string][]UserTrade, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("can't open user trades file: %s", err)
	}
	defer f.Close()

	outputFile, err := os.Create("output.csv")
	if err != nil {
		return nil, fmt.Errorf("can't create output file: %s", err)
	}

	defer outputFile.Close()

	csvr := csv.NewReader(f)
	csvOut := csv.NewWriter(outputFile)
	userTrades := make(map[int]map[string][]UserTrade)

	for {
		row, err := csvr.Read()
		if err != nil {
			if err == io.EOF {
				return userTrades, nil
			}

			return userTrades, fmt.Errorf("reading newline caused erro: %s", err)
		}
		//create UserTrade instance, then append it to slice of UserTrades
		const stocknameField = 2

		userID, _ := strconv.ParseInt(row[0], 10, bitSize)
		trades := userTrades[int(userID)][row[stocknameField]]

		tmpTrade, err := enterTradeDetails(trades, row)
		if err != nil {
			return nil, fmt.Errorf("can't parse row correctly: %s", err)
		}

		add(userTrades, int(userID), row[stocknameField], *tmpTrade)
		_ = tryFlushTradeToFile(*tmpTrade, *csvOut, bestTrades)
	}
}

func enterTradeDetails(tradesList []UserTrade, row []string) (*UserTrade, error) {
	const (
		buyField, sellField = 3, 4
		stocknameField      = 2
		nothing             = 0.0
	)

	var tmpTrade UserTrade

	buyPrice, _ := strconv.ParseFloat(row[buyField], bitSize)

	userID, err := strconv.ParseInt(row[0], 10, bitSize)
	if err != nil {
		return nil, fmt.Errorf("can't parse userID: %s", err)
	}

	if buyPrice == nothing {
		sellPrice, _ := strconv.ParseFloat(row[sellField], bitSize)

		for _, trade := range tradesList {
			if trade.buyPrice != nothing {
				tmpTrade = trade
				tmpTrade.isFinished = true
			}
		}

		tmpTrade.sellPrice = sellPrice
		tmpTrade.userID = int(userID)
		tmpTrade.stockName = row[stocknameField]
	} else {
		for _, trade := range tradesList {
			if trade.sellPrice != nothing {
				tmpTrade = trade
				tmpTrade.isFinished = true
			}
		}
		tmpTrade.buyPrice = buyPrice
		tmpTrade.userID = int(userID)
		tmpTrade.stockName = row[stocknameField]
	}

	return &tmpTrade, nil
}

func tryFlushTradeToFile(trade UserTrade, csvOut csv.Writer, bestTrades map[string]*BestTrade) error {
	if !trade.isFinished {
		return nil
	}

	const precision = 2

	revenue := trade.sellPrice - trade.buyPrice
	maxRevenue := bestTrades[trade.stockName].maxPrice - bestTrades[trade.stockName].minPrice
	super := []string{
		strconv.Itoa(trade.userID),
		trade.stockName,
		strconv.FormatFloat(revenue, 'f', precision, bitSize),
		strconv.FormatFloat(maxRevenue, 'f', precision, bitSize),
		strconv.FormatFloat(maxRevenue-revenue, 'f', precision, bitSize),
		bestTrades[trade.stockName].timeToSell,
		bestTrades[trade.stockName].timeToBuy,
	}

	err := csvOut.Write(super)
	if err != nil {
		return fmt.Errorf("can't write to output file: %s", err)
	}

	csvOut.Flush()

	return nil
}
